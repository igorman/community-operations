--- Name this issue: `FY Q | Community Team Budget Planning` ----

# Context
We need to plan the Community Relations team budget for `INSERT FINANCIAL YEAR and QUARTER`. Our target spend this quarter is `$`.

## Current Plan
This is a simplification of amounts for the quarter:

 * Christina - 
 * John - 
 * Nuritzi - 
 * Christos - 

Already set: 
 * Consortium memberships: 

### Allocadia

Here is a [spreadsheet with the current plan](https://docs.google.com/spreadsheets/d/1T1iPK9653gqPeOOLxzLx9OWOPH4Bp6iDdCcy2FeK32g/edit#gid=2052967130).

## Actual vs Budget
The following spredsheets are given to us by the Finance team at the end of each month.
 * [Month Year](https://docs.google.com/spreadsheets/d/1KMdsgjBKhFZnxpvqf55B3XBb3slNEkpzNu_hQ-Pnx-k/edit#gid=18521305)
